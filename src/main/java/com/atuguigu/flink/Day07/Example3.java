package com.atuguigu.flink.Day07;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;

import java.util.Properties;
//将数据写入发kafka
public class Example3 {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        Properties properties = new Properties();
        properties.put("bootstrap.servers","hadoop104:9092");

        env
                .readTextFile("D:\\ideaproject\\maven\\FlinkBigData1021\\src\\main\\resources\\data\\UserBehavior.csv")
                .addSink(new FlinkKafkaProducer<String>(
                        "userbehavior",
                        new SimpleStringSchema(),
                        properties
                ));
        env.execute();
    }
}
