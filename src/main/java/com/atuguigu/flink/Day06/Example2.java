package com.atuguigu.flink.Day06;


import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.cep.CEP;
import org.apache.flink.cep.PatternSelectFunction;
import org.apache.flink.cep.PatternStream;
import org.apache.flink.cep.pattern.Pattern;
import org.apache.flink.cep.pattern.conditions.SimpleCondition;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

// 使用flink-cep检测5s内连续三次登录失败
public class Example2 {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        SingleOutputStreamOperator<Event> loginStream = env
                .fromElements(
                        new Event("user1", "file", "0.0.0.1", 1000L),
                        new Event("user2", "file", "0.0.0.0", 2000L),
                        new Event("user2", "success", "0.0.0.0", 3000L),
                        new Event("user1", "file", "0.0.0.2", 4000L),
                        new Event("user1", "file", "0.0.0.3", 4500L))
                .assignTimestampsAndWatermarks(
                        WatermarkStrategy.<Event>forMonotonousTimestamps()
                                .withTimestampAssigner(new SerializableTimestampAssigner<Event>() {
                                    @Override
                                    public long extractTimestamp(Event element, long recordTimestamp) {
                                        return element.timestamp;
                                    }
                                })
                );

        //定义需要的模板
        Pattern<Event, Event> pattern = Pattern
                .<Event>begin("first")
                .where(new SimpleCondition<Event>() {
                    @Override
                    public boolean filter(Event value) throws Exception {
                        return value.enentType.equals("file");
                    }
                })
                .next("second")
                .where(new SimpleCondition<Event>() {
                    @Override
                    public boolean filter(Event value) throws Exception {
                        return value.enentType.equals("file");
                    }
                })
                .next("three")
                .where(new SimpleCondition<Event>() {
                    @Override
                    public boolean filter(Event value) throws Exception {
                        return value.enentType.equals("file");
                    }
                }).within(Time.seconds(5));
                ;


        //匹配出来的数据流
        PatternStream<Event> patternStream = CEP.pattern(loginStream.keyBy(r -> r.userId), pattern);


        //输出匹配到的数据流
        patternStream
                .select(new PatternSelectFunction<Event, String>() {
                    @Override
                    public String select(Map<String, List<Event>> map) throws Exception {
                        Event first = map.get("first").get(0);
                        Event second = map.get("second").get(0);
                        Event three = map.get("three").get(0);
                        return "用户" + first.userId + "在" + first.ipadress + ";" + second.ipadress + ";" + three.ipadress + ";" + "登录失败，可能是爬虫";
                    }
                })
                .print();




        env.execute();
    }
    public static class Event{
        public String userId;
        public String enentType;
        public String ipadress;
        public Long timestamp;

        public Event() {
        }

        public Event(String userId, String enentType, String ipadress, Long timestamp) {
            this.userId = userId;
            this.enentType = enentType;
            this.ipadress = ipadress;
            this.timestamp = timestamp;
        }

        @Override
        public String toString() {
            return "Event{" +
                    "userId='" + userId + '\'' +
                    ", enentType='" + enentType + '\'' +
                    ", ipadress='" + ipadress + '\'' +
                    ", timestamp=" + new Timestamp(timestamp) +
                    '}';
        }
    }
}
