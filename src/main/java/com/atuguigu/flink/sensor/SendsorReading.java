package com.atuguigu.flink.sensor;


import java.sql.Timestamp;

//POJO类
public class SendsorReading {
    public String id;
    public Double temperture;
    public Long timestamp;

    public SendsorReading() {
    }

    public SendsorReading(String id, Double temperture, Long timestamp) {
        this.id = id;
        this.temperture = temperture;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "SendsorReading{" +
                "id='" + id + '\'' +
                ", temperture=" + temperture +
                ", timestamp=" + new Timestamp(timestamp) +
                '}';
    }
}
