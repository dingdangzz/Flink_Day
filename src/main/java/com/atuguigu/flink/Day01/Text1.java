package com.atuguigu.flink.Day01;

import java.util.Random;
//高斯随机数范围测试。

/*-0.2631070849524352
        0.1787539754018635
        0.3584661415166255
        -0.06004305288296138
        0.40820937387262296
        -0.8431230178474206
        0.7507919174382072
        0.20033189074297592
        -2.2993120161536327
        0.8504584580187814
        -0.32165490578891354
        -0.8009324701725363
        -0.8324833895893405
        -1.6833268302190068
        0.3662688911164857
        0.47649553495545144
        0.9141418379986461
        -0.29480878061706706
        1.4370410800489408
        -0.8686258948812889
        1.0748057806549953*/
public class Text1 {
    private static boolean running=true;
    public static void main(String[] args) {

        while (running){
            Random random = new Random();
            double v = random.nextGaussian();
            System.out.println(v);
        }

    }
}
