package com.atuguigu.flink.Day02.window;


import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Exampel1 {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        env.fromElements(
                Tuple2.of("a",1),
                Tuple2.of("b",1)
        ).map(
                //将元组变为(key,a,1).(key,b,1)
                new MapFunction<Tuple2<String, Integer>, Tuple3<String,String,Integer>>() {
                    @Override
                    public Tuple3<String, String, Integer> map(Tuple2<String, Integer> value) throws Exception {
                        return Tuple3.of("key",value.f0,value.f1);
                    }
                }
        ).keyBy(
                //通过Kkey进行分组
                new KeySelector<Tuple3<String, String, Integer>, String>() {
                    @Override
                    public String getKey(Tuple3<String, String, Integer> value) throws Exception {
                        return value.f0;
                    }
                }
        ).print();


        env.execute();
    }
}
