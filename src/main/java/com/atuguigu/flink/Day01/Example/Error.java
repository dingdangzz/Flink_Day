package com.atuguigu.flink.Day01.Example;

import jdk.nashorn.internal.codegen.types.Type;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Error {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);


        DataStreamSource<String> stream = env.fromElements("white", "black", "gray");

        stream
                .flatMap(
                        (FlatMapFunction<String, String>)(s, out) -> {
                            if (s.equals("while")) {
                                out.collect(s);
                            } else if (s.equals("black")) {
                                out.collect(s);
                                out.collect(s);
                            }
                        }).print();

        env.execute();
    }
}
