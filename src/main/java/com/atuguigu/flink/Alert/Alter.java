package com.atuguigu.flink.Alert;

import java.sql.Timestamp;

public class Alter {
    public String message;
    public Long timestamp;

    public Alter() {
    }

    public Alter(String message, Long timestamp) {
        this.message = message;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Alter{" +
                "message='" + message + '\'' +
                ", timestamp=" +new Timestamp(timestamp) +
                '}';
    }
}
