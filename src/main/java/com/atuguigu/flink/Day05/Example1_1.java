package com.atuguigu.flink.Day05;

import com.atuguigu.flink.Day01.Singlesensor.SensorSource;
import com.atuguigu.flink.sensor.SendsorReading;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

// 旁路输出的例子
//当温度小于多少度的时候，从旁路输出
public class Example1_1 {

    //旁路输入
    private  static  OutputTag<String> outputTag =new OutputTag<String>("旁路输入"){};
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        SingleOutputStreamOperator<SendsorReading> result = env
                .addSource(new SensorSource())
                .process(new ProcessFunction<SendsorReading, SendsorReading>() {
                    @Override
                    public void processElement(SendsorReading value, Context ctx, Collector<SendsorReading> out) throws Exception {
                        if (value.temperture < 40) {
                            ctx.output(outputTag, "温度小于40度");
                        } else {
                            out.collect(value);
                        }
                    }
                });

        result.print();
        result.getSideOutput(outputTag).print();


        env.execute();
    }
}
