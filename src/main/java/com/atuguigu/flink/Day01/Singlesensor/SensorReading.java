package com.atuguigu.flink.Day01.Singlesensor;
public class SensorReading {
    //传感器对象。
    public String id;
    public Double temperture;
    public Long timestream;//时间戳

    public SensorReading() {
    }

    public SensorReading(String id, Double temperture, Long timestream) {
        this.id = id;
        this.temperture = temperture;
        this.timestream = timestream;
    }

    @Override
    public String toString() {
        return "SensorReading{" +
                "id='" + id + '\'' +
                ", temperture=" + temperture +
                ", timestream=" +timestream +
                '}';
    }
}
