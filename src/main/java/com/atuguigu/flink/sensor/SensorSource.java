package com.atuguigu.flink.sensor;

import org.apache.commons.math3.fitting.leastsquares.EvaluationRmsChecker;
import org.apache.flink.streaming.api.functions.source.RichParallelSourceFunction;

import java.util.Calendar;
import java.util.Random;

public class SensorSource extends RichParallelSourceFunction<SendsorReading> {

        private boolean running = true;
    @Override
    public void run(SourceContext<SendsorReading> sourceContext) throws Exception {
            //run函数用来产生数据
        //随机数发生器
        Random random = new Random();
        //生成10个传感器
        String[] sendsorIds = new String[10];
        //生成10个温度
        Double[] curFTemp = new Double[10];
        for (int i=0;i<10;i++){
            sendsorIds[i]="sensor_"+(i+1);//10个编号
            curFTemp[i]=65+(random.nextGaussian()*20);
        }

        //在循环体里面发送数据
        while (running){
            long curTime = Calendar.getInstance().getTimeInMillis();
            for (int i=0;i<10;i++){
              curFTemp[i]+=random.nextGaussian();
              sourceContext.collect(new SendsorReading(sendsorIds[i],curFTemp[i],curTime));
            }

            Thread.sleep(100);

        }



    }

    @Override
    public void cancel() {
        running=false;
    }
}
