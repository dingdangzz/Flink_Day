package com.atuguigu.flink.Alert;

import org.apache.flink.streaming.api.functions.source.SourceFunction;

import java.util.Random;
//烟雾流的数据源
public class SomkeLevelSource implements SourceFunction<SmokeLevel> {
    private Boolean running = true;
    @Override
    public void run(SourceContext<SmokeLevel> sourceContext) throws Exception {
        Random random = new Random();
        while (running){
            if(random.nextGaussian()>0.8){
                sourceContext.collect(SmokeLevel.HIGH);
            }else {
                sourceContext.collect(SmokeLevel.LOW);
            }
        }

    }

    @Override
    public void cancel() {
        running=false;

    }
}
